package app.fedilab.mobilizon;

/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.fedilab.mobilizon.client.entities.Search;
import app.fedilab.mobilizon.client.entities.data.InstanceData;
import app.fedilab.mobilizon.databinding.ActivityInstancePickerBinding;
import app.fedilab.mobilizon.drawer.InstanceAdapter;
import app.fedilab.mobilizon.viewmodel.SearchInstancesVM;


public class InstancePickerActivity extends AppCompatActivity {


    private ActivityInstancePickerBinding binding;
    private List<InstanceData.Instance> instances;
    private boolean flag_loading;
    private String max_id;
    private Search search;
    private InstanceAdapter instanceAdapter;
    private SearchInstancesVM searchInstancesVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityInstancePickerBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        search = new Search();
        search.setCount("10");
        search.setStart(max_id);
        search.setSort("-createdAt");

        max_id = "0";
        instances = new ArrayList<>();

        binding.loader.setVisibility(View.VISIBLE);
        binding.lvInstances.setVisibility(View.GONE);
        flag_loading = true;
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(InstancePickerActivity.this);
        searchInstancesVM = new ViewModelProvider(InstancePickerActivity.this).get(SearchInstancesVM.class);
        instanceAdapter = new InstanceAdapter(instances);
        binding.lvInstances.setAdapter(instanceAdapter);
        binding.lvInstances.setLayoutManager(mLayoutManager);

        binding.lvInstances.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    if (firstVisibleItem + visibleItemCount == totalItemCount) {
                        if (!flag_loading) {
                            flag_loading = true;
                            search.setStart(max_id);
                            searchInstancesVM.get(search).observe(InstancePickerActivity.this, instanceData -> manageVIewInstance(instanceData));
                            binding.loadingNextInstances.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.loadingNextInstances.setVisibility(View.GONE);
                    }
                }
            }
        });


        searchInstancesVM.get(search).observe(InstancePickerActivity.this, this::manageVIewInstance);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_instance, menu);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                max_id = "0";
                if (query != null && query.trim().length() > 0) {
                    search.setSearch(query.trim());
                } else {
                    search.setSearch(null);
                }
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(InstancePickerActivity.this);
                instanceAdapter = new InstanceAdapter(instances);
                binding.lvInstances.setAdapter(instanceAdapter);
                binding.lvInstances.setLayoutManager(mLayoutManager);
                search.setStart(max_id);
                binding.loader.setVisibility(View.VISIBLE);
                binding.lvInstances.setVisibility(View.GONE);
                searchInstancesVM.get(search).observe(InstancePickerActivity.this, instanceData -> manageVIewInstance(instanceData));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void manageVIewInstance(InstanceData instanceData) {
        binding.loader.setVisibility(View.GONE);
        binding.loadingNextInstances.setVisibility(View.GONE);
        flag_loading = false;
        if (instanceData == null || (instanceData.data.size() == 0 && this.instances.size() == 0)) {
            binding.noAction.setVisibility(View.VISIBLE);
            binding.lvInstances.setVisibility(View.GONE);
            return;
        }
        int oldPosition = this.instances.size();
        List<InstanceData.Instance> instances = instanceData.data;
        this.instances.addAll(instances);
        instanceAdapter.notifyItemRangeInserted(oldPosition, instances.size());
        binding.noAction.setVisibility(View.GONE);
        binding.lvInstances.setVisibility(View.VISIBLE);
        max_id = String.valueOf(Integer.parseInt(max_id) + 10);
    }
}
