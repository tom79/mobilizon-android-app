package app.fedilab.mobilizon.client;

import app.fedilab.mobilizon.client.entities.WellKnownNodeinfo;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface MobilizonService {


    //Server settings
    @GET(".well-known/nodeinfo")
    Call<WellKnownNodeinfo> getWellKnownNodeinfo();

    @GET("{nodeInfoPath}")
    Call<WellKnownNodeinfo.NodeInfo> getNodeinfo(@Path(value = "nodeInfoPath", encoded = true) String nodeInfoPath);

}
