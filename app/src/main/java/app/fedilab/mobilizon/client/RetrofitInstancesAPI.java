package app.fedilab.mobilizon.client;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */


import java.io.IOException;

import app.fedilab.mobilizon.client.entities.Search;
import app.fedilab.mobilizon.client.entities.data.InstanceData;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstancesAPI {


    private InstancesService init() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://instances.joinmobilizon.org/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(InstancesService.class);
    }


    /**
     * Get NodeInfo
     *
     * @return APIResponse
     */
    public InstanceData search(Search search) {
        InstancesService instancesService = init();
        try {
            Call<InstanceData> instanceDataCall = instancesService.getInstances(search.getStart(), search.getCount(), search.getSearch(), search.getSort());
            Response<InstanceData> response = instanceDataCall.execute();
            if (response.isSuccessful() && response.body() != null) {
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
