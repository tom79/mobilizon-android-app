package app.fedilab.mobilizon.drawer;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.fedilab.mobilizon.R;
import app.fedilab.mobilizon.client.entities.data.InstanceData;
import app.fedilab.mobilizon.client.entities.data.InstanceData.Instance;

import static android.app.Activity.RESULT_OK;


public class InstanceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Instance> instances;

    private Context context;

    public InstanceAdapter(List<Instance> instances) {
        this.instances = instances;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        return new ViewHolder(layoutInflater.inflate(R.layout.drawer_instance, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final Instance instance = instances.get(position);
        final ViewHolder holder = (ViewHolder) viewHolder;

        if (instance.getShortDescription() != null && instance.getShortDescription().trim().length() > 0) {
            holder.description.setText(instance.getShortDescription());
            holder.description.setVisibility(View.VISIBLE);
        } else {
            holder.description.setVisibility(View.GONE);
        }

        holder.name.setText(instance.getName());
        holder.host.setText(instance.getHost());


        StringBuilder languages = new StringBuilder();
        if (instance.getLanguages() != null && instance.getLanguages().size() > 0) {
            for (InstanceData.Language language : instance.getLanguages()) {
                languages.append(language.getDisplayName()).append(" ");
            }
        }
        if (languages.toString().trim().length() == 0) {
            holder.languages.setVisibility(View.GONE);
        } else {
            holder.languages.setText(languages);
            holder.languages.setVisibility(View.VISIBLE);
        }

        holder.version.setText(context.getString(R.string.version, String.valueOf(instance.getVersion())));
        holder.users.setText(context.getString(R.string.users, String.valueOf(instance.getTotalUsers())));
        holder.groups.setText(context.getString(R.string.groups, String.valueOf(instance.getTotalLocalGroups())));
        holder.local_events.setText(context.getString(R.string.local_events, String.valueOf(instance.getTotalLocalEvents())));

        holder.pickup.setOnClickListener(v -> {
            Intent data = new Intent();
            String instanceHost = instance.getHost();
            data.setData(Uri.parse(instanceHost));
            ((Activity) context).setResult(RESULT_OK, data);
            ((Activity) context).finish();
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return instances.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        Button pickup;
        TextView name, host, description;
        TextView version, local_events, languages, groups, users;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pickup = itemView.findViewById(R.id.pickup);
            name = itemView.findViewById(R.id.name);
            host = itemView.findViewById(R.id.host);
            description = itemView.findViewById(R.id.description);
            local_events = itemView.findViewById(R.id.local_events);
            version = itemView.findViewById(R.id.version);
            languages = itemView.findViewById(R.id.languages);
            groups = itemView.findViewById(R.id.groups);
            users = itemView.findViewById(R.id.users);
        }
    }
}