Mi a Mobilizon?

A Mobilizon egy internetes eszköz, amely segít az Ön eseményei, profiljai és csoportjai kezelésében.

- Az Ön eseményei
A Mobilizon alkalmazással részletes oldalt hozhat létre az eseményéhez, közzéteheti és megoszthatja azt.
Kereshet eseményeket is kulcsszó, helyszín vagy dátum szerint, részt vehet eseményeken (akár fiók nélkül is) és hozzáadhatja azokat a napirendjéhez.

- Az Ön profiljai
Egy Mobilizon példányon történő fiók beállításával számos profil létrehozására (például személyes, üzleti, hobbi, aktivizmus stb.), események szervezésére és csoportok kezelésére lesz lehetősége.
Mielőtt fiókot hozna létre egy példányon, ne felejtse el megismerni a példány működését a „névjegy” oldalának elolvasásával, hogy jobban megértse a szabályait és az irányelveit.

- Az Ön csoportjai
A Mobilizon alkalmazásban minden csoportnak van nyilvános oldala, ahol megtekintheti a csoport legújabb hozzászólásait és nyilvános eseményeit.
Ha meghívják egy csoporthoz való csatlakozásra, a tagok részt vehetnek a megbeszélésekben, és kezelhetik a közös erőforrásmappát (például egy együttműködő íróeszközre mutató hivatkozásokat, egy wikit stb.)
