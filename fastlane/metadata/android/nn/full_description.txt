Kva er Mobilizon?

Mobilizon er eit nettverkty for å handtera hendingane, profilane og gruppene dine.

- Hendingane dine
På Mobilizon kan du laga ei detaljert side for hendinga di, og du kan publisera og dela ho.
Du kan òg søkja etter hendingar med søkjeord, stad eller dato, melda deg på hendingar (til og med utan ein brukarkonto) og leggja dei til i kalenderen din.

- Profilane dine
Når du lagar ein brukarkonto på ein Mobilizon- nettstad, kan du laga fleire profilar (td. personleg, jobb, hobby, aktivisme osb.), organisera hendingar og handtera grupper.
Før du lagar ein brukarkonto på ein nettstad, bør du lesa "om"-sida der for å forstå reglane og retningslinene på nettstaden.

- Gruppene dine
På Mobilizon har kvar gruppe ei side der du kan sjå dei siste innlegga og hendingane i gruppa.
Når du blir invitert til ei gruppe, kan du vera med på ordskifte, og styra ei felles mappe med ressursar (td. lenker til eit felles skriveverkty, ein wiki osb.)
